@namespace url(http://www.w3.org/1999/xhtml);

@-moz-document domain("html.duckduckgo.com") {	
	.serp__results{max-width:800px;}
	
	#header {
		background-color: #de5833;
	}
	
	#header select {
		color: #fff;
		font-weight: bold;
	}
	
	#header select:hover {
		color:red;
	}
	
	#search_button_homepage {
		background-color:#5b9e4d;
	}
}
