// ==UserScript==
// @name        Pausar WebMs
// @version     1
// @namespace		genoskill
// @author      genesis bustamante
// @description pausa videos y les baja el volumen.
// @run-at      document-end
// @include     /^https?://.+\.(?:mp4|webm).*/
// @include		/^https?://(chan|idol).sankakucomplex.com/post/show/*/
// ==/UserScript==
console.log('[genesis script]: capturando videos');
var videos = document.getElementsByTagName('video');

if (videos.length > 0) {
	console.log('[genesis script]: videos encontrados = ' + videos.length);
	for (i = 0; i < videos.length; ++i) {
		videos[i].volume = 0.1;
		videos[i].autoplay = false;
		videos[i].pause();
		videos[i].loop = true;
	}
	setTimeout(function(){ videos[0].play(); }, 500);
}
