// ==UserScript==
// @name     Youtube - mostrar duracion videos
// @namespace   genoskill
// @version  1
// @grant    none
// @include	http://www.youtube.com/*
// @include	https://www.youtube.com/*
// @exclude	https://www.youtube.com/feed/subscriptions
// ==/UserScript==
//var tiempos_videos = document.getElementsByTagName('video-time');
//window.onload = function() {
var hojas_estilos = document.styleSheets;
var ind_ultimo_estilo = hojas_estilos.length - 1;
var ultimo_estilo = hojas_estilos[ind_ultimo_estilo];
var ind_nueva_regla = ultimo_estilo.length; // Se insertará al final para que sobreescriba cualquier otra regla
ultimo_estilo.insertRule('.video-time { position: static }', ind_nueva_regla); // Hace que los tiempos en los thumbnails no se escondan
//}
console.log('(greasemonkey, youtube genesis) indice del ultimo estilo = ' + ind_ultimo_estilo);
