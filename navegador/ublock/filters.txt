! 2021-05-11 https://stackoverflow.com
stackoverflow.com##.js-dismissable-hero.z-nav.b0.r0.l0.ps-fixed.bc-black-100.bt.bs-sm.bg-black-025

! 2021-05-13 medium.com
@@||cdn-client.medium.com/lite/static/js/Post.*.chunk.js$script,domain=selamjie.medium.com

! 2021-05-28 https://superuser.com
superuser.com##.js-consent-banner

! 2021-05-28 https://askubuntu.com
askubuntu.com##.js-consent-banner

! 2021-06-01 https://serverfault.com
serverfault.com##.js-consent-banner

! 2021-04-29 https://www.redhat.com
www.redhat.com##.redhat-cookie-banner-wrapper

! 2021-04-29 https://chan.sankakucomplex.com
chan.sankakucomplex.com##div#sp1.scad > div

! 2021-04-29 https://www.reddit.com
www.reddit.com##.premium-banner

! 2021-05-02 https://chan.sankakucomplex.com
chan.sankakucomplex.com##div#sp1.scad

! 2021-05-06 https://chan.sankakucomplex.com
||www.sankakucomplex.com/wp-content/*$image,domain=chan.sankakucomplex.com
chan.sankakucomplex.com###headerlogo
chan.sankakucomplex.com###share

! 2021-05-14 https://yewtu.be
||yewtu.be/js/*$script,1p
@@||yewtu.be/js/video.min.js$script,1p

! 2022-10-28 https://chan.sankakucomplex.com
chan.sankakucomplex.com###has-mail-notice

! 2023-01-18 https://www.deepl.com
www.deepl.com###headlessui-tabs-tab-1
www.deepl.com###headlessui-tabs-tab-2 > .cardButton--VaX9A
www.deepl.com##.dl_top_element--wide.dl_header_menu_v2
www.deepl.com##.dl_header--sticky.dl_header

! 2023-02-08 https://www.studylight.org
www.studylight.org##.side
www.studylight.org##.fs-15.ptb10.mb20.jc-evenly.flexbox-row-nowrap.bl-lightgrey.tl-lightgrey.revcatch-client-hide
www.studylight.org##.jc-between.flexbox-row-nowrap.noselect.ptb2.clickable.menubar
www.studylight.org##.logo


! GENERAL
||*.ssl.hwcdn.net/templates/pm/$image
*$image,domain=urbandictionary.com|es.answers.yahoo.com
*$script,domain=plos.org|answers.com|es.answers.yahoo.com|theoi.com|medium.com|ekoru.org

! un script que pesa mucho
||journalofdairyscience.org/wro/*product.js

! scripts de facebook en TheFreeDictionary
||connect.facebook.net/$script

! THIRD PARTY BASURA
||youtube.com$domain=yourveganfallacyis.com|collinsdictionary.com|wikihow.com
||soundcloud.com/$domain=yourveganfallacyis.com
||facebook.com/$domain=thefreedictionary.com
||gravatar.com/avatar/$image,domain=stackoverflow.com
||gstatic.com$script,domain=google.com
||twitter.com$domain=gelbooru.com
||youtube.com/api/stats/$domain=beatport.com
||platform.twitter.com$domain=gelbooru.com

! GOOGLE APIS
||apis.google.com$domain=oxfordlearnersdictionaries.com|yourdictionary.com|wikihow.com|google.com|soundcloud.com
||accounts.google.com$domain=oxfordlearnersdictionaries.com|yourdictionary.com|wikihow.com
||ajax.googleapis.com/ajax/$domain=onelook.com
||fonts.googleapis.com
||notifications.google.com/$domain=google.com
||play.google.com/$domain=google.com


!! *************************************************
!! ****************** SITIOS ***********************
!! *************************************************

! Beatport
||beatport.com/image_size/250x250$image

! Bible Verses
www.biblegateway.com##.bga-top.leader.bga > .bga-column > .bga-wrap > .bga-placement

! DeepL
www.deepl.com##.lmt__docTrans-tab-container

! Devochdelia
!||http://devochdelia.cl/assets/style/user.css
!||http://devochdelia.cl/assets/style/about.css

! Encrypted Google
||encrypted.google.com/xjs/$script
||ogs.google.com

! Fate Gand Order Wikia
||vignette.wikia.nocookie.net/fategrandorder/images/*/Wiki-background

! Ecosia
||cdn-static.ecosia.org/images/css/*.css

! Gelbooru
||gelbooru.com/script/tryt.js
||gelbooru.com/thumbnails/0C/$image
||gelbooru.com/user_avatars/avatar$image
||ads.exoclick.com
||gelbooru.com/extras/shirt.jpg$image

!Github
||githubassets.com/assets/behaviors-*.js
||githubassets.com/assets/frameworks-*.js
||githubassets.com/assets/github-*.js

! Google
||google.com/xjs$script

! Med Lexicon
||medilexicon.com/structure/styles/$stylesheet
||medilexicon.com/structure/javascript/$script
http://www.medilexicon.com/structure/images/logo/hceegfei.png

! Medium

! Merriam Webster
||merriam-webster.com/assets/$script
||merriam-webster.com/assets/mw/static/newsletter/$image
||merriam-webster.com/assets/mw/images/quiz/$image
||merriam-webster.com/assets/mw/static/app-css-images/global/$image
www.merriam-webster.com##.border-box.home-top-creative-cont > .abl-m320-t728-d728.abl
www.merriam-webster.com###social-sidebar-container > ul

! Memrise
||memrise.com/img/*/from/uploads/profiles/$image
||memrise.com/accounts/img/placeholders/$image

! Musicbrainz
||staticbrainz.org/MB/styles/icons-*.css
||staticbrainz.org/$script

! Nutritionfacts
||nutritionfacts.org/$script
||assets.zendesk.com/$script
||use.typekit.net/$script

! Oxford Living Dictionaries
||oup.useremarkable.com/production/images/uploads/$image
||blog.oxforddictionaries.com/wp-content/uploads/$image
||audio.oxforddictionaries.com/en/mp3/$media
||en.oxforddictionaries.com/assets/$script
en.oxforddictionaries.com##.banbox
en.oxforddictionaries.com###sidebar
en.oxforddictionaries.com##.cookie-container
en.oxforddictionaries.com##.banbox

! Pixiv
!! recomendados de pixiv
||source.pixiv.net/www/js/bundle/illust_recommend.*.js
||pixiv.net/rpc/index.php?mode=get_recommend_users_and_works&user_id=*
!! otros trabajos del usuario en pixiv
||pixiv.net/rpc/index.php?mode=profile_module_illusts&user_id=*
!! Wtf aparecieron nuevos request en pixiv
||pixiv.net/rpc/recommender.php?type=illust&sample_illusts
!||pixiv.net/rpc/illust_list.php?illust_ids

! PubMed
||ncbi.nlm.nih.gov/core/alerts/alerts.js
||static.pubmed.gov/coreweb/images/ncbi/footer/$image
||ncbi.nlm.nih.gov/$script
||static.pubmed.gov/$script
@@static.pubmed.gov/core/jig/*/jig.min.js

! RAE
dle.rae.es###patrocinio
dle.rae.es###enclave
dle.rae.es##.dle-header
dle.rae.es###superfish-1
dle.rae.es##.pat_dcha
dle.rae.es##.pat_izda

! Rational Wiki
||rationalwiki.org/w/load.php$script
https://rationalwiki.org/w/images/b/be/Brain_watermark_light.png

! Reddit
||reddit.com/chat/minimize
||reddit.com/api/request_promo

! rule34.xxx
||rule34.xxx//icame.png$image
||trafficsan.com
||rule34.xxx/bf/$image

! Sankakucomplex
||*.addthis.com/$script
||js-agent.newrelic.com/$script
||sankakucomplex.com/wp-content/uploads/
!!||chan.sankakucomplex.com/stylesheets/style.css
!!||chan.sankakucomplex.com/javascripts/app.js
!||chan.sankakucomplex.com/p/pre.js
chan.sankakucomplex.com###news-ticker
chan.sankakucomplex.com###headerlogo
chan.sankakucomplex.com###has-mail-notice

! Soundcloud
||sndcdn.com/avatars-*20x20$image
||sndcdn.com/avatars-*50x50$image
||sndcdn.com/avatars-*500x500$image
||style.sndcdn.com/css/interstate-$stylesheet
||sndcdn.com/artworks-*50x50$image
||sndcdn.com/artworks-*200x200$image

! Stackoverflow
!||cdn.sstatic.net/$stylesheet
stackoverflow.com###js-gdpr-consent-banner
stackoverflow.com##.js-consent-banner

! Stack*
unix.stackexchange.com##.py8.px16.mx-auto.wmx12.jc-space-between.grid
unix.stackexchange.com###js-gdpr-consent-banner
superuser.com###js-gdpr-consent-banner

! TheFreeDictionary
es.thefreedictionary.com###loginBlock
es.thefreedictionary.com##.logo-holder

! Urban Dictionary
www.urbandictionary.com##.mug-ad
/sp.js
urbandictionary.store/cart.json
www.urbandictionary.com###UD_ROS_728x90_ATF_Flex
www.urbandictionary.com###UD_ROS_300x600_ATF_Flex

! Wikipedia
||*.wikipedia.org/w/$script

!! Wikimedia
||commons.wikimedia.org/w/load.php?lang=en&modules=Spinner%2CdataValues%2Cjquery$script
||commons.wikimedia.org/w/index.php?title=MediaWiki:Gadget-ImageAnnotator.js$script
||commons.wikimedia.org/w/load.php?lang=en&modules=dataValues%2Cjquery$script
||commons.wikimedia.org/w/load.php?lang=en&modules=ext.echo.styles.badge|ext.tmh.thumbnail.styles$stylesheet

! Wiktionary
!! Qué carajo hace esto aparte de gastar datos??
!||*.wiktionary.org/w/load.php?lang=*&modules=ext.centralNotice.choiceData$script
!! Qué hace esto??
!||*.wiktionary.org/w/load.php?lang=*&modules=Spinner%2Cjquery%2Coojs$script
!! Wiktionary EN
!||en.wiktionary.org/w/load.php?lang=en&modules=startup&only=scripts$script
!||en.wiktionary.org/w/load.php?lang=en&modules=ext.echo.styles.badge$stylesheet
!! Wikt De
!||de.wiktionary.org/w/load.php?lang=de&modules=ext.cite.styles$stylesheet
!||de.wiktionary.org/w/load.php?lang=de&modules=ext.echo.styles.badge$stylesheet
!||de.wiktionary.org/w/load.php?lang=de&modules=ext.TemplateWizard$script

! WWWJDIC (diccionario japones)
||http://assets.languagepod101.com/dictionary/japanese/audiomp3.php

! Yahoo Answers
||s.yimg.com/$script
||ct.yimg.com/cy/$image

! YOUR dictionary
||yourdictionary.com$image
||assets.pinterest.com$domain=yourdictionary.com
||disqus.com$domain=yourdictionary.com

! Youtube
!! avatares de la campana de mensajes
!!||googleusercontent.com/*photo.jpg
!!||yt3.ggpht.com/$image
!! thumbnails de barra de tiempo
!!||ytimg.com/sb/$image
!!||ytimg.com/vi/$image
www.youtube.com##.comment-author-thumbnail.yt-thumb

! Yummly
||mapi.yummly.com/mapi/v16/content/feed
||lh3.googleusercontent.com$domain=yummly.com

