#!/bin/bash
# Este script es para silenciar y desilenciar el volumen del PC.
# Se debe vincular a un comando xej. Alt+Shift+M

CURRENT_STATE=`amixer get Master | egrep 'Playback.*?\[o' | egrep -m 1 -o '\[o.+\]' | tr -d "\n"`

if [[ $CURRENT_STATE == '[on]' ]]; then
    amixer set 'Master' mute
else
    amixer set Master unmute
fi
