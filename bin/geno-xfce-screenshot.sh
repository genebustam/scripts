#!/bin/sh

# TODO: cambiar de gestor de clipboard para automatizar el proceso así:
# Al apretar tecla "Impr" este script debe guardar la screenshot a la carpeta y
# luego abrir la imagen con feh.

momento="$(date +%Y-%m-%d_%H-%M-%S)"
#momento_strip_last_6=${momento%??????}
#nombre_archivo="xfce4-screenshooter_$momento_strip_last_6"
nombre_archivo="xfce4-ss_$momento"
ruta=~/Pictures/screenshot
ruta_completa="$ruta/$nombre_archivo.png"

xfce4-screenshooter --fullscreen --mouse --save "$ruta_completa" && /usr/bin/feh "$ruta_completa" && exit 0

geno-play-error-sound.sh
exit 1
