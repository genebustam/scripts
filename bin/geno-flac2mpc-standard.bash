#!/bin/bash

mkdir mpc-st
mkdir flac2wav-tmp
for i in *.[fF][l-L][a-A][c-C];do ffmpeg -n -i "$i" "./flac2wav-tmp/${i%.[fF][l-L][a-A][c-C]}.wav"; mpcenc --standard "./flac2wav-tmp/${i%.[fF][l-L][a-A][c-C]}.wav" "mpc-st/${i%.[fF][l-L][a-A][c-C]}.mpc";done
