#!/bin/sh
# --action = acción que se activa apretando 0
# --action1 = acción que se activa apretando 1
# --action2 = acción que se activa apretando 2

# acción 0 = envía archivo visible a la papelera.
# acción 1 = mueve archivo a otra parte.
# acción 2 = crea una copia del archivo.
feh \
--action "gio trash %F" \
--action1 "~/.local/bin/geno-zenity-move-file %F" \
--action2 ";~/.local/bin/geno-zenity-save-as %F" "$@"


exit
