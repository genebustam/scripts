#!/bin/bash

# sysinfo_page - Un escript para producir un archivo comic CBZ a partir de imagen JPG O PNG

##### Functions

usage() { echo "Usage: ${0##*/} [-f <filtro>] [-s <ancho imagen>] [-q <calidad>] [-b brillo-contraste] [-g <gamma>] input_files ..." 1>&2; exit 1; }

##### Constants

CADENAOPCION=s:q:f:b:g:

## Default values
nuevo_ancho=
calidad=
filtro=
brillo_contraste=
gamma=


## Extrayendo valores
while getopts $CADENAOPCION opc
    do
        case "$opc" in
            s)nuevo_ancho=$OPTARG ;;
            q)calidad=$OPTARG ;;
            f)filtro=$OPTARG ;;
            b)brillo_contraste=$OPTARG ;;
            g)gamma=$OPTARG ;;
            *)printf "NADA ESCRITO (o alguna otra razon)"; exit 1 ;;
        esac
done

## Actualizando valores
[ -z "$1" ] && usage && exit 1


##### Main
shift "$((OPTIND - 1))"
ruta_carpeta_actual=$(dirname "$1")
[ $ruta_carpeta_actual == "." ] && ruta_carpeta_actual=$(pwd)

nombre_archivo=$(basename "$ruta_carpeta_actual").cbz
echo "  "ruta primer archivo: "$ruta_carpeta_actual"
echo "  "ruta carpeta actual: "$(pwd)"

## Si no hay ningún parametro nombrado, las imágenes no se procesan.
if [ -z $nuevo_ancho ] && [ -z $calidad ] && [ -z $filtro ] && [ -z $brillo_contraste ] && [ -z $gamma ] ; then
    rm "$ruta_carpeta_actual/$nombre_archivo" &>/dev/null
    7z a -tzip "$ruta_carpeta_actual/$nombre_archivo" "$@"
    
## ¡A procesar imágenes!
else

    ## Default values
    [ -z $nuevo_ancho ] && nuevo_ancho=1200
    
    [ -z $calidad ] && calidad=80
    
    [ -z $filtro ] && filtro=Sinc
    
    [ -z $brillo_contraste ] && brillo_contraste=+0x+0
    
    [ -z $gamma ] && gamma=1
    
    nombre_carpeta_tmp="$nuevo_ancho"px"$calidad"q
    rm -r "$ruta_carpeta_actual/$nombre_carpeta_tmp" &>/dev/null
    mkdir "$ruta_carpeta_actual/$nombre_carpeta_tmp"

    while [ -n "$1" ]
        do
            echo "    --->" "$1" start
            nasr="${1##*/}" ## nombre archivo sin ruta
            convert "$1" -gamma "$gamma" -brightness-contrast $brillo_contraste -filter "$filtro" -quality "$calidad" -resize "$nuevo_ancho>" "$ruta_carpeta_actual/$nombre_carpeta_tmp/${nasr%.*g}.jpg"
            shift 1
    done
    rm "$ruta_carpeta_actual/$nombre_archivo" &>/dev/null
    7z a -tzip "$ruta_carpeta_actual/$nombre_archivo" -w "$ruta_carpeta_actual/$nombre_carpeta_tmp/."
fi


