#!/bin/sh

# No guarda archivo, sino que mueve archivo.
nombre_archivo="$(basename $1)"
nombre_carpeta="$(dirname $1)"
nueva_ruta=$(cd "$nombre_carpeta" && zenity --file-selection \
	--filename="$nombre_archivo" --directory --title="Mover archivo" \
	--confirm-overwrite) && mv "$1" "$nueva_ruta"

echo $nueva_ruta

exit

