#!/bin/sh

tmux_file="$HOME/.tmux.conf"

grep_find_conf_sessions=$(grep --only-matching -e "^new-session -s [[:lower:]]\+" $tmux_file | grep --only-matching -e "[[:lower:]]\+$")

grep_find_live_sessions=$(tmux list-sessions | grep --only-matching --basic-regexp -e "^[[:alnum:]]\+")

choice=0
#while [ $choice != 'q' ]; do
i=1
    echo ''
    echo '------------- Escoge una sesión -------------'
    echo ''
    echo '--tmux.conf------------------'
	for sesion in $grep_find_conf_sessions; do echo " $i. $sesion"; i=$((i+1)); done
    echo '--list-sessions--------------'
	for sesion in $grep_find_live_sessions; do echo " $i. $sesion"; i=$((i+1)); done
    echo '-----------------------------'
    echo " $i. Nueva sesión con 3 shells"
	echo " $((i+1)). Salir."
    echo ''
    echo "Please enter your choice: \c"

read choice
z=$i
zz=$((z+1))
#zzz=$((zz+1))
case "$choice" in
	# Detach a process from Sh (not Bash):
	# Alternativa a 'disown':
	#	https://unix.stackexchange.com/questions/147760/detach-a-process-from-sh-not-bash-or-disown-for-sh-not-bash/608872#608872
	$z) nohup /bin/st -g 120x36+200+60 -t "Nueva sesión" /usr/bin/tmux new-session -n bash_1 \; new-window -d -n bash_2 \; new-window -d -n bash_3 >/dev/null 2>&1 & ;;
	$zz) exit 0 ;;
	*) set $grep_find_conf_sessions $grep_find_live_sessions
	   [ "$choice" -le $zz ] && [ "$choice" -gt 0 ] && shift $((choice-1)) \
		   && nohup /bin/st -g 161x42+55+8 -t $1 /usr/bin/tmux attach-session -d -t $1 >/dev/null 2>&1 & ;;
esac
#done
sleep 1
