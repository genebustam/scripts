#!/usr/bin/python2.7
#autor: Genesis Bustamante Lillo
#2014-06-08 23:04:41
# Calcula el bitrate promedio de los archivos `.ogg` en la carpeta actual, ya sean Vorbis o OPUS.
import os

#executing and saving ogginfo information
print "current directory: " + os.getcwdu().split('/')[-1]
command_bitrate = os.popen('ogginfo *.og[ga]|grep Average|grep -o -E [0-9]\.*[.][0-9]*')
command_time = os.popen('ogginfo *.og[ga]|grep Playback|grep -o -E [0-9]*[m][:][0-9]*[.][0-9]*')

#creating python lists
bitrates = command_bitrate.read()
bitrates = bitrates.splitlines()

#ammount of items
n = len(bitrates)

#converting to actual float numbers
for pos in range(n):
	bitrates[pos] = float( bitrates[pos] )

#legacy reference information
average_bitrate = sum(bitrates)/n
print "average_bitrate = " + str(average_bitrate)

#list with song times in seconds
song_times = command_time.read()
song_times = song_times.splitlines()
for pos in range(n):
	ts = song_times[pos].split('m:')
	song_times[pos] = int(ts[0])*60 + float(ts[1])

#improved Average Bitrate
def mult(na, nb):
	return na*nb
song_sizes = map(mult, bitrates, song_times)
Improved_Average_Bitrate = sum(song_sizes) / sum(song_times)
print "Improved_Average_Bitrate = " + str(Improved_Average_Bitrate)
