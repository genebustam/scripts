#!/bin/sh

#itera sobre todos los argumentos que en este caso serian archivos
for i in $*;
#usando mv para cambiarle el nombre a cada archivo, se saca el ".oga"
#y se agrega ".ogg" al final
do mv "$i" "${i%.oga}.ogg";
#finaliza una iteracion
done
