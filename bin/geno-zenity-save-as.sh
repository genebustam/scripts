#!/bin/sh

# Guarda copia de archivo.
nombre_archivo="$(basename $1)"
nombre_carpeta="$(dirname $1)"
nueva_ruta=$(cd "$nombre_carpeta" && zenity --file-selection --directory \
	--filename="$nombre_archivo" --title="Guardar copia de archivo" \
	--confirm-overwrite) && cp "$1" "$nueva_ruta"

exit

