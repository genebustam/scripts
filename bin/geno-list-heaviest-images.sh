#!/bin/sh
#
# lista las 6 (ver 'tail') imágenes más grandes de la carpeta actual, y RECURSIVAMENTE.

find -type f -regextype grep -regex ".*\.\(jpg\|png\|webp\|tiff\|bmp\|jpeg\|webp\)" \
    -printf '%s %p\n' | sort --numeric-sort | tail -6 | cut --delimiter=" "   \
    --fields=2- | awk '{printf("\"%s\"\n", $0)}' | tee archivos_más_largos.txt

## lo mismo pero sin comillas envolventes usando awk.
#find -type f -regextype grep -regex ".*\.\(jpg\|png\|webp\|tiff\|bmp\|jpeg\)" -printf '%s %p\n' | sort --numeric-sort | tail -6 | cut --delimiter=" " --fields=2-


