#!/bin/bash

##### Functions

usage() { echo "Usage: ${0##*/} <bitrate>" 1>&2; exit 1; }

#### Main

[ -z "$1" ] && usage && exit 1

if `echo $1 | grep -E [[:digit:]]*[\.]?[[:digit:]]+$`; then
        echo $1 is not a number
        exit 1
else
        echo $1 is a number, GOOD.
fi

[ "$1" -gt 256 ] && echo $1 es muy grande, FAIL. && exit 1

[ "$1" -lt 64 ] && echo $1 es muy chico, FAIL. && exit 1

quality=$1
tmpfolder="opus$quality"
filesfolder=$(basename "$(pwd)")
tmpfolder="$filesfolder opus$quality"

mkdir "$tmpfolder"
for i in *.[fF][lL][aA][cC];do opusenc --discard-pictures --padding 2048 --bitrate "$quality" --comment "COMMENT=00000000001111111111000000000011111111110000000000111111111100000000001111111111000000000011111111110000000000111111111100000000001111111111000000000011111111110000000000111111111100000000001111111111000000000011111111110000000000111111111100000000001111111111000000000011111111110000000000111111111100000000001111111111000000000011111111110000000000111111111100000000001111111111000000000011111111110000000000111111111100000000001111111111000000000011111111110000000000111111111100000000001111111111" "$i" "$tmpfolder/${i%.[fF][lL][aA][cC]}.opus";done;

#cd "$tmpfolder";
#vorbisgain *.ogg;

#bitrate;
