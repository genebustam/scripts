#!/bin/bash

##### Functions

usage() { echo "Usage: ${0##*/} <quality>" 1>&2; exit 1; }

#### Main

[ -z "$1" ] && usage && exit 1

if `echo $1 | grep -E [[:digit:]]*[\.]?[[:digit:]]+$`; then
	echo $1 is not a number
	exit 1
else
	echo $1 is a number, GOOD.
fi

[ "$1" -gt 10 ] && echo $1 es muy grande, FAIL. && exit 1

[ "$1" -lt 1 ] && echo $1 es muy chico, FAIL. && exit 1

quality=$1
tmpfolder="ogg-q$quality"
filesfolder=$(basename "$(pwd)")
tmpfolder="$filesfolder oggq$quality"

mkdir "$tmpfolder"
for i in *.[fF][l-L][a-A][c-C];do oggenc -q "$quality" "$i" -o "$tmpfolder/${i%.[fF][l-L][a-A][c-C]}.ogg";done;

#cd "$tmpfolder";
vorbisgain "$tmpfolder"/*.ogg;

bitrate;
