#!/bin/sh
# Este script envía todos los archivos de una carpeta a un archivo `.cbz` con el nombre de la carpeta.
# Envía los archivos sin su estructura de carpeta, y recursivamente.

## funciones

usage() {
    echo "Uso: ${0##*/} carpeta" 1>&2;
    exit 1;
}

## extraer parámetros

[ -z "$1" ] && usage && exit 1

## comprimir con 'zip'

while [ -n "$1" ]
do
    echo "    -->" "$1" "start"
    if [ -d "$1" ]; then
        # el nombre de la carpeta pero sin slash al final
        sin_slash=`echo $1 | sed 's#/*$##'`
        echo "    -->" "$sin_slash" "sin slash"
        zip --display-bytes --recurse-paths --junk-paths --compression-method deflate -9 "$sin_slash.cbz" "$sin_slash"
        gio trash "$1"
    fi
    shift 1
done

