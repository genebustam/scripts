#!/bin/sh
# Fuente: https://ubuntuforums.org/showthread.php?t=1970507
# Modificado por mí, Genesis Bustamante, para ser usado con /bin/sh.

main_menu()
{
    read cursetting < /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
    read getspd < /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
    read cursetting2 < /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
    read getspd2 < /sys/devices/system/cpu/cpu1/cpufreq/scaling_cur_freq
    curspd=$(echo "scale=4;"$getspd"/"1000000 | bc)
    curspd2=$(echo "scale=4;"$getspd2"/"1000000 | bc)
    echo ""
    echo ""
    echo "-----------------CPU Settings---------------------"
    echo "1. Allow user to set CPU speed (UserSpace)."
    echo "2. Set CPU to Minimum (Powersave) setting."
    echo "3. Set CPU to Low (Conservative) setting."
    echo "4. Set CPU to Medium (OnDemand) setting."
    echo "5. Set CPU to High (Performance) setting."
    echo "6. View CPUID info string."
    echo "7. View Temperature sensor info string."
    echo "8. Exit."
    echo "--------------------------------------------------"
	echo "        Current CPU Setting - "$cursetting" (0), "$cursetting2" (1)"
	echo "        Current CPU Speed - "$curspd"GHz (0), "$curspd2"GHz (1)"
    echo ""
    echo "Please enter your choice: \c"
}

press_enter()
{
    echo ""
    echo -n "Press Enter to continue."
    read nothing
}    

while [ "$choice" != "8" ]; do
	main_menu
	read choice
	case "$choice" in
		1)read linea < /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies
			set $linea
			i=1
			for frecuencia in $linea; do echo $i")"$frecuencia" \c"; i=$((i+1)); done
			echo "\nEnter your choice of CPU frequency: \c"
			read eleccion
			[ "$eleccion" -le "$#" ] && [ "$eleccion" -gt "0" ] && shift \
				$(($eleccion-1)) && echo userspace | sudo tee \
				/sys/devices/system/cpu/cpu*/cpufreq/scaling_governor && echo \
				$1 | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_setspeed
			set -- ;;
		2)echo powersave | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ;;
		3)echo conservative | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ;;
		4)echo ondemand | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ;;
		5)echo performance | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor ;;
		6)echo "\n\n"
			cpuid;
			press_enter ;;
		7)echo "\n\n"
			sensors;
			press_enter ;;
		q)choice=8 ;;
		8)exit ;;
		*)echo "Please enter the NUMBER of your choice: \c" ;;
	esac
done
