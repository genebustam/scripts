#!/bin/bash

mkdir mpc-ex
mkdir flac2wav-tmp
for i in *.[fF][l-L][a-A][c-C];do ffmpeg -n -i "$i" "./flac2wav-tmp/${i%.[fF][l-L][a-A][c-C]}.wav"; mpcenc --extreme "./flac2wav-tmp/${i%.[fF][l-L][a-A][c-C]}.wav" "mpc-ex/${i%.[fF][l-L][a-A][c-C]}.mpc";done
