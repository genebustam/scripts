#!/bin/sh
# Divide un archivo FLAC en múltilpes archivos FLAC con el archivo CUE asumido que existe.

currentfolder=$(basename "$(pwd)")

splitfolder="$currentfolder SPLIT"

mkdir "$splitfolder"

#shnsplit -f *.cue -o flac *.flac

cuebreakpoints *.cue | shnsplit -o flac *.flac;

cuetag.sh *.cue split*.flac


mv split*.flac "$splitfolder"

for fff in "$splitfolder"/*.flac; do
TITLE=`metaflac "$fff" --show-tag=TITLE | sed s/.*=//g`
TRACKNUMBER=`metaflac "$fff" --show-tag=TRACKNUMBER | sed s/.*=//g`
mv "$fff" "$splitfolder/`printf %02g $TRACKNUMBER` - $TITLE.flac";
done;
