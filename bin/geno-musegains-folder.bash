#!/bin/bash
# Script para descubrir tags de volume gain en archivos Musepack.
mkdir ogg-gains;
for i in *.mpc;do mpcdec "$i" "ogg-gains/${i%.mpc}.wav";done;

mkdir ogg-gains/ogg-q6;

for i in ./ogg-gains/*.wav;do oggenc -q 6 "$i" -o "ogg-gains/ogg-q6/$(basename "${i%.wav}").ogg";done;

vorbisgain -f ./ogg-gains/ogg-q6/*.ogg;

puddletag .;

rm -rf ogg-gains;